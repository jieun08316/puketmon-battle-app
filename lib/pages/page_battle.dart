import 'package:flutter/material.dart';
import 'package:my_pet_app/pages/page_background_setting.dart';
import 'package:my_pet_app/pages/page_index.dart';

class PageBattle extends StatefulWidget {
  const PageBattle({super.key});

  @override
  State<PageBattle> createState() => _PageBattleState();
}

bool _isStart = false;

class _PageBattleState extends State<PageBattle> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Row(
          children: [
            Image.asset(
              'assets/images/logo.png',
              width: 100,
              alignment: Alignment.centerRight,
            ),
            Text(
              '  푸켓몬 배틀',
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
                fontWeight: FontWeight.w600,
                shadows: [
                  Shadow(
                    blurRadius: 60.0,
                    color: Colors.white,
                    offset: Offset(2.0, 2.0),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      body: backgroundContainer(
        context: context,
        //📲 이미지 경로를 맞게 기입합니다
        //📲 특히! 확장자 대소문까지 맞아야 하니 확인해야합니다
        imagePath: 'assets/images/background_sub.jpg',
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,

          children: [
            Padding(
                padding: EdgeInsetsDirectional.only(
                    start: 0.0, top: 10.0, end: 0.0, bottom: 10.0)),
            const Text(
              ' Puketmon Fight! ',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                color: Colors.white,
                shadows: [
                  Shadow(
                    blurRadius: 20.0,
                    color: Colors.blueAccent,
                    offset: Offset(5.0, 5.0),
                  ),
                ],
              ),
              textAlign: TextAlign.center,
            ),
            Column(
              children: [
                Container(
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return PageIndex();
                          },
                        ),
                      );

                      setState(() {
                        _isStart = false;
                      });
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Colors.amber,
                      onPrimary: Colors.orange,
                    ),
                    child: const Text(
                      '대기실로 이동',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        shadows: [
                          Shadow(
                            blurRadius: 5.0,
                            color: Colors.black38,
                            offset: Offset(2.0, 2.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
