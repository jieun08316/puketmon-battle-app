import 'package:flutter/material.dart';
import 'package:my_pet_app/components/component_appbar_nomal.dart';
import 'package:my_pet_app/pages/page_background_setting.dart';
import 'package:my_pet_app/pages/page_battle.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  bool _isStart = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('푸켓몬스터',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
      ),
      //📲
      // body 부분에 설정한 위젯을 가져옵니다
      body: backgroundContainer(
        context: context,
        //📲 이미지 경로를 맞게 기입합니다
        //📲 특히! 확장자 대소문까지 맞아야 하니 확인해야합니다
        imagePath: 'assets/images/background.jpg',
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,

          children: [
          Column(
            children: [
              Column(
                children: [
                  Container(
                    child: Row(
                      children: [
                        Column(
                          children: [
                            SizedBox(
                              width: 200,
                              child: Image.asset(
                                'assets/images/pikachu.png',
                              ),
                            ),
                            const Text(
                              '피카츄<Pikachu>',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                shadows: [
                                  Shadow(
                                    blurRadius: 5.0,
                                    color: Colors.blueAccent,
                                    offset: Offset(2.0, 2.0),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ), //피카츄 end
                        Container(
                          child: Column(
                            children: [
                              SizedBox(
                                  child: Image.asset(
                                    'assets/images/mue.png',
                                    width: 200,
                                  )),
                              const Text(
                                '뮤<Mue>',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 5.0,
                                      color: Colors.blueAccent,
                                      offset: Offset(2.0, 2.0),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ) //뮤 end
                      ],
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Container(
                    child: Row(
                      children: [
                        Column(
                          children: [
                            SizedBox(
                              width: 200,
                              child: Image.asset(
                                'assets/images/jammanbo.png',
                              ),
                            ),
                            const Text(
                              '잠만보<Jammanbo>',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                shadows: [
                                  Shadow(
                                    blurRadius: 5.0,
                                    color: Colors.blueAccent,
                                    offset: Offset(2.0, 2.0),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Container(
                          child: Column(
                            children: [
                              SizedBox(
                                  child: Image.asset(
                                    'assets/images/anibugi.png',
                                    width: 200,
                                  )),
                              const Text(
                                '어니부기<Asnibugi>',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 5.0,
                                      color: Colors.blueAccent,
                                      offset: Offset(2.0, 2.0),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
          Container(
            child: Column(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return PageBattle();
                        },
                      ),
                    );

                    setState(() {
                      _isStart = false;
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blueAccent,
                    onPrimary: Colors.black54,
                  ),
                  child: const Text(
                    '결투',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      shadows: [
                        Shadow(
                          blurRadius: 5.0,
                          color: Colors.black38,
                          offset: Offset(2.0, 2.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: [
              Padding(
                  padding: EdgeInsetsDirectional.only(
                      start: 0.0, top: 20.0, end: 0.0, bottom: 0.0)),
              Container(
                child: SizedBox(
                  child: Image.asset(
                    'assets/images/logo.png',
                    width: 300,

                  ),
                ),
              )
            ],
          )
          ],
        ),
      ),
    );
  }
}
