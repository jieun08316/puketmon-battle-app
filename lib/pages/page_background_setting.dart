import 'package:flutter/material.dart';

Widget backgroundContainer(
    {required BuildContext context,
      required Widget child,
      required String imagePath}) {
  return Container(
    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage(imagePath),
        //📲 이 부분은 사진에 맞게 맞춰주시면 됩니다
        //📲 페이지 마다 따로 설정해야 한다면 파라미터로 빼도 됩니다
        fit: BoxFit.fill,
      ),
    ),
    //📲 Column 위젯을 사용하는 것을 권장합니다
    child: child,
  );
}