import 'package:flutter/material.dart';

class ComponentAppbarNormal extends StatefulWidget {
  const ComponentAppbarNormal({super.key, required this.title});

  final String title;

  @override
  State<ComponentAppbarNormal> createState() => _ComponentAppbarNormalState();
}

class _ComponentAppbarNormalState extends State<ComponentAppbarNormal> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.blue,
      title: Text(widget.title,
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.white),
      ),
    );
  }
}
